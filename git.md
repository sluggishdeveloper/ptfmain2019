# git 手順

## ローカルレポジトリ(ローカルの開発ディレクトリ)を指定
git init
## 変更内容をステージに載せる
git add .
* この場合はディレクトリの中身全部
## 変更内容を決定する
git commit -m 'msg'
## 変更内容をリモートリポジトリ(開発環境ディレクトリ)に送信
git push -u origin master
* -u は変更箇所だけ、 -f は強制的にローカルのものを全部あげる
* git push -u origin :repsitory name これでRemoteにあるリポジトリーを強制削除して上書き
## リモートから引っ張ってくる
git pull origin master

## ローカルでのコミットの取り消し
git reset

___
## ブランチ関連
### 現在のBranchの一覧確認
- git branch
### リモートBranchの確認
- git branch -a
### ブランチの作成
- git branch branch名
### ブランチの変更
- git checkout ブランチ名
* git checkout -b branch名　：これで新たなBranchを作成して、そのBranchに切り替える
___

## Branchの統合
1. マージ先のBranchに入る
- git checkout Merge先のBranch名
2. マージする
- git merge Merge元のBanch名
___

## 変更履歴の確認
git log
## ローカルにて変更があったファイルを確認する
git status
##