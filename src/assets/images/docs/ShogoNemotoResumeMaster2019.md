<span style="font-family:;"></span>
# Shogo Nemoto / Resume
<div style="text-align:right;color:navy;" > TEL: +81 90-7634-7340  / Email: shogonemoto@gmail.com </div>

## #Objectives
As someone who has extensive experience from grand system design to field experience with proof of concept work, I am confident to manage and lead a global development team to create suitable application to address client needs and social challenges.
From hardware related SLAM or futuristic UX service on display or speech recognition functionality, we can do this by connecting ICT devices, updating software and products to automate the workforce of tomorrow.

## #Profile
1. <span style="color:navy;"> Entrepreneurship Mindset </span> - experienced self motivated Business & Service Development Specialist
2. <span style="color:navy;"> All Around Player </span>- being flexible to conduct from planning to business development to system/UX design
3. <span style="color:navy;"> General knowledge and field experience </span>- from upcoming autonomous driving software, robot recognition and deep learning to hardware components
4. <span style="color:navy;">Strong design thinking skill </span>- solving and finding out the futuristic user interface and foreseeing technological trends creatively
5. <span style="color:navy;">Resulted oriented creative team player</span> who is passionate about contributing to a better society

## #Work Experience

### __Softbank Robotics Corp.__
#### Service Development and Project Manager/ UI and UX Designer <span style="font-size:80%;color:navy;float:right;"> Aug 2017 – Present / Tokyo, Japan </span>
- Lead the world rare Elevator and robot cooperation functionality development projects
- Managed or planned Android OS based Pepper robot application including SLAM based mobility function, automating speech and facial recognition and entertainment function for retail stores
- Conducted UX design for Proof of Concept based Pepper robot application by Ad hoc client requests
- Effectively initiated the government or private organization to propel robot application development project in proof of concept based needs
- Managed and led a global development team to plan and create overall robot application

### __SANYO DENKI Inc.__
#### Project Manager, Sales Representative / UI and UX Desinger <span style="font-size:80%;color:navy;float:right;"> Jul 2013 – Jul2017 / Tokyo, Japan </span>
- Worked closely with the corporate headquarters and decision maker to plan and incorporated the work effective IT tools for the entire corporation
- Adopted User focused Groupware and CRM for the corporation that resulted in better customer data management
- Planned and incorporated social networking system to focus on collecting useful ideas for new product development
- Managed and handled Sanyo Ecommerce shop and technically integrated its IT inventory systems
- Effectively negotiated and made marketing entry into major electronics manufacturer that carry UPS, Fan and servo motor

### __Stockpools Inc.__
#### Front End Developer / Web Designer <span style="font-size:80%;color:navy;float:right;">Jul 2012 - Jun 2013 / Vancouver Canada
- Promoted and created website events and social networking systems that increased customer engagement by 120%
- Effectively designed and developed high click-through graphic banners and that increased company's ad revenue

### __Revenue Automation Inc.__
#### Front End Developer / Web Designer <span style="font-size:80%;color:navy;float:right;"> May 2011 - Jun 2012 / Vancouver,Canada </span>
- Provided full support to the marketing team and addressed all the technical and design problems clients experienced
- Built highly functional and customer-focused websites for clients that resulted in a lift in web traffic by 110%
- Created personalized HTML Marketing Automation emails for deployment and customer nurture campaigns
- Managed client relationship and coordinated projects with the project managers, partners and agencies


### __FLS Inc.__
#### School Manager/ Customer Service <span style="font-size:80%;color:navy;float:right;"> Dec 2007 - Jul 2009 / Manila, Philippines/ Sydney, Australia </span>
- Improved school's overall marketing campaigns and efforts to recruit students

### __Creek and River, Co. Ltd.__
#### Web Director / Sales Representative <span style="font-size:80%;color:navy;float:right;"> Apr 2006 - Sept 2007 / Tokyo, Japan </span>
- Handled online marketing customer engagement ads and created employment graphic ads for specific client, and increased the number of job applicants by more than 150% in 1 month
- Managed clients web projects and delivered high results meeting deadlines and expectations
- Identified the company's potential human resource needs and placed skilled-matching workers that improve client's overall company productivity

## #Education
### __Vancouver Institute of Media and Arts (Vanarts)__
#### Diploma, Web Development, Interactive Design and Online Marketing<span style="font-size:80%;color:navy;float:right;"> Sep 2009 - Apr 2011 / Vancouver,Canada </span>
### __Aoyama Gakuin University__
#### BA in Education, Specialization in elementary students’ education<span style="font-size:80%;color:navy;float:right;"> Apr 2002 - Mar 2006 / Tokyo, Japan </span>

## Skill
|  |  |  | |
|:-----------|:------------|:-------------|:-------------|
| Language   || Japanese    | Mother language             |
|            || English     | Business                    |
| Software   | OS          | Windows 10   | for Office and Design  |
|            |             | Mac OS X      | for Office and Development   |
|            |             | Linux Ubuntu 16.04 | for Development |
|            | Office      | MS Office    |              |
|            |             | G-suite      |              |
|            | Design      | Graphics     | Illustrator, Photoshop, Indesign  |
|            |             | 3D-model     | Fusion 360   |
| Programming| Markup      | HTML5, CSS/SASS             |
|            | UI/UX       | Javascript   | Library(Jquery,Vue,etc) |
|            | Web         | Static Generator | Gatsby   |
|            |             | CMS              | Wordpress, Joomla, Drupal      |
|            | Backend     | PHP          |              |
|            |             | Node.js      |              |
|            | Database    | MYSQL        |              |
|            |             | Posgre SQL   |              |
| Hardware   | License     | Electrical wiring license 2nd grade | Sep 2016 |
|            |             | Certified Master Electrician |Apr 2017 |

