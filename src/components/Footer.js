import React from 'react'

var email = "weathercockconsluting@gmail.com";
class Footer extends React.Component {
    render() {
        return (
            <section id="footer">
                <ul className="icons">
                    <li><a href="http://www.facebook.com/shougo.nemoto" className="icon alt fa-facebook"><span className="label">Facebook</span></a></li>
                    <li><a href="https://gitlab.com/sluggishdeveloper" className="icon alt fa-github"><span className="label">Gitlab</span></a></li>
                    <li><a onClick={this.copy} href="#./" className="icon alt fa-envelope"><span className="label">Email</span></a></li>
                </ul>
                <ul className="copyright">
                    <li>&copy; SHOGO NEMOTO </li>
                    <li>Design integrated from: <a href="http://html5up.net">HTML5 UP</a>. build with <a href="https://www.gatsbyjs.org" >Gatsby.js</a></li>
                </ul>
            </section>
        )
    }
    copy() {
        alert('Cotanct email address has copied to your clipboards!! Please paste one at your email :' + email );
      }
}

export default Footer
