import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <section id="regHeader">
                <div id="headerbar">
                    <div className="home">
                        <a href="../" id="onCli">
                            { /*
                            <svg className="homeBG"></svg>
                            <svg>
                                <circle cx="10" cy="10" r="40" stroke="orange" stroke-width="1" fill="yellow" />
                            </svg>
                            */}
                            <span>Home</span>
                        </a>
                    </div>
                        {/*
                        <div className="menu">
                            <a href="../" id="onCli">
                                <span className="home">Home</span>                
                            </a>
                        </div>
                        // Inside menu
                        {/*  
                            - Logo has to be placed top left
                            <a href="#one" className="button scrolly">
                                <svg> This has to be driven by Illustrator</svg>
                            </a>
                            - Navigation menu has to be top right 
                            <ul className="actions">
                                <li><a href="#one" className="button scrolly">Discover</a></li>
                                <li><a href="#two" className="button scrolly">Discover</a></li>
                                <li><a href="#three" className="button scrolly">Discover</a></li>
                                <li><a href="#four" className="button scrolly">Discover</a></li>
                                <li><a href="#footer" className="button scrolly">contact</a></li>
                            </ul>
                        */}
                </div>
            </section>
        )
    }
}
export default Header
