import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <section id="header">
                <div id="headerBar">
                    <div className="home">
                        <a href="../" id="onCli">
                            { /*
                            <svg className="homeBG"></svg>
                            */}
                            <svg>
                                <circle cx="10" cy="10" r="40" stroke="orange" fill="yellow" />
                            </svg>
                        </a>
                    </div>
                    { /* menu bar
                    <div className="menu">
                        <a href="../" id="onCli">
                            <span>menu</span>                
                        </a>
                    </div>
                    */}
                </div>
                <div className="inner">
                    {/* unused */}
                    <span id='topIcon'></span>
                    <h1>
                        <strong>Shogo Nemoto's Portfolio</strong>
                    </h1>
                    <h2>"Business Torch Bearer "</h2>
                    <h3>Illustrating better future by enlightening people with technology</h3>
                    {/* clouds unused
                    <div class="clouds"></div>
                    */}
                </div>
            </section>
        )
    }
}

export default Header
