import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <section id="header">
                <div className="inner">
                    <span className="icon major fa-cloud"></span>
                    <h1>
                        <strong>"Business Torch Bearer "</strong>
                        <br></br>Shogo Nemoto's 
                    </h1>
                    <h2>Portfolio and his milestones</h2>
                    <h3>Illustrating better future by enlightening people with technology</h3>
                    {/*  
                    - Logo has to be placed top left
                    <a href="#one" className="button scrolly">
                        <svg> This has to be driven by Illustrator</svg>
                    </a>
                    - Navigation menu has to be top right 
                    <ul className="actions">
                        <li><a href="#one" className="button scrolly">Discover</a></li>
                        <li><a href="#two" className="button scrolly">Discover</a></li>
                        <li><a href="#three" className="button scrolly">Discover</a></li>
                        <li><a href="#four" className="button scrolly">Discover</a></li>
                        <li><a href="#footer" className="button scrolly">contact</a></li>
                    </ul>
                     */}
                </div>
            </section>
        )
    }
}

export default Header
