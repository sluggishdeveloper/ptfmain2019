import React from 'react'

import mobility from '../assets/images/gallery/mobility.png';
import poc from '../assets/images/gallery/poc.png';
import app from '../assets/images/gallery/app.png';
import web from '../assets/images/gallery/web.png';
import system from '../assets/images/gallery/system.png';
import MA from '../assets/images/gallery/MA.png';


const email = "weathercockconsluting@gmail.com";

class Gallery extends React.Component {
    render() {
        return (
            <section id="three" className="main style2 special" >
                <div className="grid-wrapper">
                    <div className="col-12">
                        <header>
                            <h2>Gallery Section</h2>
                        </header>
                    </div>

                    <div className="col-12">
                        <h3>Robots &amp; Apps</h3>
                    </div>
                    <div className="col-4">
                        <span className="image fit">
                            <img src={mobility} alt="mobility" />
                            <a onClick={this.copy} href="#./gallery" className="button">
                                <p>Robot mobility </p>
                            </a>
                        </span>
                    </div>
                    <div className="col-4">
                        <span className="image fit"><img src={poc} alt="" />
                            <a onClick={() => { alert('woooooooot?'); }} href="#./gallery" className="button">
                                <p>Proof of Concept work </p>
                            </a>
                        </span>
                    </div>
                    <div className="col-4">
                        <span className="image fit"><img src={app} alt="" />
                            <a onClick={this.togglePopup.bind(this)} href="#./gallery" className="button">
                                {/* this.state.showPopup ?
                                    <PopWindow
                                        text='Close Me'
                                        closePopup={this.togglePopup.bind(this)}
                                    />
                                    : null
                                */}
                                <p>Applicarions</p>
                            </a>
                        </span>
                    </div>

                    <div className="col-12">
                        <h3>Web &amp; Systems</h3>
                    </div>
                    <div className="col-4">
                        <span className="image fit"><img src={web} alt="" />
                            <a href="#./gallery" className="button">
                                <p>Web site &amp; E-commerce </p>
                            </a>
                        </span>
                    </div>
                    <div className="col-4">
                        <span className="image fit"><img src={system} alt="" />
                            <a href="#./gallery" className="button">
                                <p>Corporate Web systems </p>
                            </a>
                        </span>
                    </div>
                    <div className="col-4">
                        <span className="image fit"><img src={MA} alt="" />
                            <a href="#./gallery" className="button">
                                <p>Marketing Automation </p>
                            </a>
                        </span>
                    </div>
                </div>
            </section >
        )
    }
    copy() {
        alert(email + "is email address");
    }
    /* Controling popupstate */
    constructor() {
        super();
        this.statae = {
            showPopup: false
        };
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
}
/* Popping up HTML window : referred url : https://codepen.io/bastianalbers/pen/PWBYvz*/
class PopWindow extends React.Component {
    render() {
        return (
            <div className='popup'>
                <div className='popup_inner'>
                    <h1>{this.props.text}</h1>
                    <button onClick={this.props.closePopup}>close me</button>
                </div>
            </div>
        );
    }
}

export default Gallery