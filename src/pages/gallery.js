import React from 'react'
import Helmet from 'react-helmet'
// import Layout from '../components/regLayout'
import Layout from '../components/galLayout'


const Generic = (props) => (
    <Layout>
        <Helmet>
            <title>Gallery - Shogo Nemoto Portfolio</title>
            <meta name="description" content="This is Shogo Nemoto's gallery pages" />
            <script src="https://kit.fontawesome.com/712239b42b.js" crossorigin="anonymous"></script>
        </Helmet>
    </Layout>
)
export default Generic