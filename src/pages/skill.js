import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/regLayout'

/* For importing images
import pic11 from '../assets/images/pic11.jpg'
*/

/* For receiving session date to display suitable content
var session = null;
*/

/* Layout definition
Gallery function has to be CSS Grid
*/

/*
Implementing functionality
- Popup Letter
- Popup fulliages
- Image data retrieved from google phpto or firestorage
*/

// import pic04 from '../assets/images/pic04.jpg'; // this has to be the main images to popup

const Generic = (props) => (
    <Layout>
        <Helmet>
            <title>Capabilities - Shogo Nemoto Portfolio</title>
            <meta name="description" content="this is gallery pages" />
            <script src="https://kit.fontawesome.com/712239b42b.js" crossorigin="anonymous"></script>
        </Helmet>
                {/* This is a Skill section page */}
                <section id="one" className="main style3 special">
                    <div className="container">
                        <header>
                            <h2>Capabilities</h2>
                        </header>
                        <h3>Language</h3>
                        <ul className="major-icons">
                            <li>
                                <span className="icon sk jp" alt="Native Japanese"></span>
                                <br></br>
                                <span className="special">Native Japanese</span>
                            </li>
                            <li>
                                <span className="icon sk en" alt="Canadian English"></span>
                                <br></br>
                                <span className="special">Business English</span>
                            </li>
                        </ul>
                        <h3>Software</h3>
                        <ul className="major-icons">
                            <li><span className="icon sk microsoft"></span></li>
                            <li><span className="icon sk apple"></span></li>
                            <li><span className="icon sk ubuntu"></span></li>
                            <li><span className="icon style4 major sk illustrator"></span></li>
                            <li><span className="icon style5 major sk photoshop"></span></li>
                            { /*
                            <li><span className="icon style6 major sk indesign"></span></li>
                            */}
                        </ul>
                        <h3>Programming</h3>
                        <h4>Front-End</h4>
                        <ul className="major-icons">
                            <li><span className="icon style1 major sk html5"></span></li>
                            <li><span className="icon style2 major sk css-3"></span></li>
                            { /* 
                            <li><span className="icon style3 major sk javascript"></span></li>
                            */}
                            <li><span className="icon style4 major sk jquery"></span></li>
                            <li><span className="icon style6 major sk react"></span></li>
                            <li><span className="icon style3 major sk wordpress"></span></li>
                        </ul>
                        <h4>Back-End / Others</h4>
                        <ul className="major-icons">
                            <li><span className="icon style2 major sk php"></span></li>
                            <li><span className="icon style3 major sk nodejs"></span></li>
                            <li><span className="icon style5 major sk android"></span></li>
                            <li><span className="icon style2 major sk firebase"></span></li>
                            <li><span className="icon style3 major sk MySQL"></span></li>
                        </ul>
                        <h3>Hardware</h3>
                        <ul className="major-icons">
                            <li>
                                <span className="icon style2 major fa-bolt"></span>
                            </li>
                            <li className="comments">
                                - Certified Master Electrician<br></br>
                                - Certified Electrician license holder in Japan 
                            </li>
                        </ul>
                    </div>
                </section>

    </Layout>
)

export default Generic