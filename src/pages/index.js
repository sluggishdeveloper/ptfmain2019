import React from "react";
import {Helmet} from "react-helmet";

import Layout from '../components/layout';

import gallery from '../assets/images/gallery.svg'
import skills from '../assets/images/skills.svg'
import samurai from '../assets/images/Samurai.svg'

class Homepage extends React.Component {
    render() {
        // const siteTitle = "Shogo Nemoto Portfolio";
        const fontawesomeCDN = "https://kit.fontawesome.com/712239b42b.js";
        return (
            <Layout>
                { /* 
                <Helmet 
                    title={siteTitle}
                    meta={
                        [{name:'description',content:'portfolio 2019 main pages'}]
                    }               
                />
                */ }
                { /* Write direct in HTML */}
                <Helmet>
                    <title>Portfolio 2019</title>
                    <meta name="description" content="This is portfolio 2019"></meta>
                    <script src={fontawesomeCDN} crossorigin="anonymous"></script>
                </Helmet>
                <section id="one" className="main style1">
                    <header className="major">
                            <h2 className="h2ctr">My Idea</h2>
                    </header>
                    <div className="grid-wrapper">
                        <div className="col-6">
                            <h3>I am creating business opportunity with service development and UI/UX Design.</h3>
                            <p>
                                Since my childhood, I love to see something that I don't know.  It is derived from my nature of curiosity and creative mind set. Getting involved in new idea always stimulate your capabilities and be my passsion. I have been inspired by various business field and industry as different roles throughout my career.This transformed me as a more thoughtful and knowledgeable individual to see things from different perspective. I am always passionate for any type of business opportunity that evolve human society. Let's make the world another step forward with technology.
                            </p>
                        </div>
                        <div className="col-6">
                            <span className="image fit"><img className="samurai" src={samurai} alt="torchbearer images" /></span>
                        </div>
                    </div>
                </section>
                {/* This is an idea section */}
                <section id="one" className="main style2">
                    <header className="major">
                        <h2 className="h2ctr">Capabilities</h2>
                    </header>
                    <div className="grid-wrapper">
                        <div className="col-6">
                            <div className="container">
                                <h3>Roles</h3>
                                <ol className="comments">
                                    <li>Business &amp; Service Development</li>
                                    <li>Project Managements</li>
                                    <li>Business / Market / Products research</li>
                                    <li>UI/ UX Design</li>
                                    <li>Development supports for teams</li>
                                </ol>
                            </div>
                        </div>

                        <div className="col-6">
                            <a href="gallery" id="onCli" className="button">Check out my Past experiences</a>
                            <a href="gallery">
                            <span className="image fit"><img src={gallery} alt="" /></span>
                            </a>
                        </div>

                        <div className="col-6">
                            <div className="container">
                                <h3>Fields</h3>
                                <ol className="comments">
                                    <li>Industrial Procucts</li>
                                    <li>Web and E-Commerce</li>
                                    <li>Digital marketing</li>
                                    <li>Corpotate system planning</li>
                                    <li>Robots and its recognition</li>
                                    <li>Advanced software, and Data Managements</li>
                                </ol>
                            </div>
                        </div>
                        <div className="col-6">
                            <a href="skill" id="onCli" className="button">Acquired skills</a>
                            <a href="skill">
                            <span className="image fit"><img src={skills} alt="" /></span>
                            </a>
                        </div>
                    </div>
                </section>
                {/* This is an MileStone section, could be written in ReactSlider or Anime.js otherwise ommit it */}
                
                <section id="two" className="main style3">
                    <div className="grid-wrapper">
                        <div className="col-6">
                            <header className="major">
                                <h2>Check out my resume</h2>
                            </header>
                            <p>Resume is available in PDF / .md files</p>
                            <ul className="major-icons">
                                <li>
                                    <a href="https://drive.google.com/uc?id=1vNvkzdLQJ6Hwrnk4m5Lwpa-q7t6HilPN" target="_new" rel="noopeners">
                                        <span className="icon style1 major fa-file-pdf">
                                        </span>
                                        <br></br>
                                        <span>
                                            PDF
                                        </span>
                                    </a>
                                </li>
                                <li> 
                                    <a href="https://drive.google.com/uc?id=1lpEyjWzwAZ2DrjR26X8XM-XhlliCbmlX" target="_new" rel="noopener">
                                        <span className="icon style2 major fa-file-code">
                                        </span>
                                        <br></br>
                                        <span>
                                            Markdown(.md)
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-6">
                            <header className="major">
                                <h2>Drop me a line</h2>
                            </header>
                            <span>Email address </span>
                            <br></br>
                            <q>weathercockconsulting@gmail.com</q>
                            <ul className="major-icons">
                                <li>
                                    <span className="icon style1 major fa-paper-plane">
                                    </span>
                                    <br></br>
                                    <span>
                                        Email
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default Homepage;