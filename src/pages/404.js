import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/regLayout'

const NotFoundPage = () => (
  <Layout>
    <Helmet>
      <title>Gallery - Shogo Nemoto Portfolio</title>
      <meta name="description" content="this is gallery pages" />
      <script src="https://kit.fontawesome.com/712239b42b.js" crossorigin="anonymous"></script>
    </Helmet>
    <section id="notFound">
      <div className="notFound">
        <h1>NOT FOUND</h1>
        <p>You just hit a route that doesn&#39;t exist... </p>
        <a href="../" className="button">Back to Home</a>
      </div>
    </section>
  </Layout>
)

export default NotFoundPage
