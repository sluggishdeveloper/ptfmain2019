# Shogo Nemoto Portfolio

Shogo Nemoto portfolio based on the Photon by HTML5 UP and gatsby photon theme.

## Objectives

Displaying Shogo Nemoto's career and works for job application purposes. Due to the reason for this, it is not commercial uses.

## Adopted technologies

### Front-end
- HTML5
- CSS : SCSS
- Javascript : React

### Others
- Gatsby
- Firebase hosting

### Urls
[Portfolio] (https://ptf2019main.firebaseapp.com/)
** Detailed tab on the gallery page on portfolio will not be shown because of including clients information. Please feel free to contact me if you need further information. I will share you a detail later**
